package tn.insat.jebouquine.data.entity;

/**
 * Created by Devcartha on 12/18/2015.
 */
public class Admin {
    private String login;
    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
